# -*- coding: utf-8 -*-
from __future__ import print_function

import re
import urllib
import json
import time
import datetime
from collections import Counter


def request(link):
    response = urllib.urlopen(link)
    return json.loads(response.read())


def is_old(date_str, days):
    date_str = re.sub(r'[A-Za-z]', ' ', str(date_str))
    date = datetime.datetime.strptime(date_str.strip(), '%Y-%m-%d %H:%M:%S')
    now = datetime.datetime.now()
    delta = now - datetime.timedelta(days=days)
    return delta > date


def stats_items(response, days):
    data = Counter()
    for item in response:
        if item['state'] == 'open':
            data['open'] += 1
            if is_old(item['created_at'], days):
                data['old'] += 1
        elif item['state'] == 'closed':
            data['close'] += 1
    return data


def author_commit(response):
    data = Counter()
    for item in response:
        name = item['commit']['author']['name']
        data[name] += 1
    return data


def print_result(result):
    line = '-------------'
    print(line)
    print('Open:  ', result['open'])
    print('Close: ', result['close'])
    print('Old:   ', result['old'])
    print(line)


def parse(link, view, end_time=None):
    page = 1
    rate = 100
    result = Counter()
    while True:
        if end_time and end_time <= datetime.datetime.now():
            break
        page_link = '&page=%s&per_page=%s' % (page, rate)
        response = request(link + page_link)
        if isinstance(response, dict):
            print('Msg: ', response['message'])
            break
        if view == 'count':
            data = stats_items(response, 30)
            result += data
        if view == 'table':
            data = author_commit(response)
            result += data
        if len(response) < rate:
            break
        page += 1
    return result


class Main:

    """

    :arg
    repo - string, link to repository
    start - string, date to start.
        Format: %Y-%m-%d %H:%M:%S
        Example: 2017-07-07 13:10:15.
        Default: None
    end - string, date to end.
        Format: %Y-%m-%d %H:%M:%S
        Example: 2017-07-07 13:10:15.
        Default: None
    branch - string, branch name.
        Default: master

    """

    def __init__(self, repo, start=None, end=None, branch=None):
        self.format = '%Y-%m-%d %H:%M:%S'
        if end:
            self.end = datetime.datetime.strptime(end, self.format)
        else:
            self.end = end
        self.branch = branch
        if start:
            self.begin(start)
        print('Program Start ', time.asctime())
        self.service = 'https://api.github.com/repos/%s' % repo
        self.links = {'pull_request': '/pulls?state=all',
                      'issue': '/issues?state=all',
                      'commit': '/commits?'}
        self.commits()
        self.pull_requests()
        self.issues()
        print('Program End ', time.asctime())

    def begin(self, start_time):
        try:
            start = time.strptime(start_time, self.format)
        except TypeError:
            return
        now = time.strptime(time.asctime())
        delta = time.mktime(start) - time.mktime(now)
        if delta > 0:
            time.sleep(delta)

    def commits(self):
        print('Get Commit.....')
        if self.branch:
            self.links['commit'] = self.links['commit'] + 'sha=' + self.branch
        result = parse(self.service + self.links['commit'], 'table', self.end)
        print(' -*- Commit -*- ')
        for item in result.most_common(30):
            print('{0:20} ==> {1:3d}'.format(item[0], item[1]))

    def pull_requests(self):
        print('Get Pull Requests.....')
        result = parse(self.service + self.links['pull_request'], 'count', self.end)
        print(' -*- Pull Request -*- ')
        print_result(result)

    def issues(self):
        print('Get Issues.....')
        result = parse(self.service + self.links['issue'], 'count', self.end)
        print(' -*- Issue -*- ')
        print_result(result)


if __name__ == '__main__':
    Main('unitedstates/congress-legislators')
